console.log("owo")
window.addEventListener('DOMContentLoaded', async ()=> {
   const api_url = "http://localhost:8000/api/states/";
   try {
        const response = await fetch(api_url)
        if (response.ok) {
            const data = await response.json();
            const selectTag = document.getElementById('state');
            for (let state of data.states) {
                let option = document.createElement('option')
                option.value = Object.values(state)
                option.innerHTML = Object.keys(state)
                selectTag.appendChild(option)

            }
            const formTag = document.querySelector('#create-location-form')
            formTag.addEventListener('submit', async (event) => {
                event.preventDefault();
                const formData = new FormData(formTag);
                const json = JSON.stringify(Object.fromEntries(formData));
                const locationUrl = 'http://localhost:8000/api/locations/';
                const fetchConfig = {
                method: "post",
                body: json,
                headers: {
                    'Content-Type': 'application/json',
                },
                };
                const response = await fetch(locationUrl, fetchConfig);
                if (response.ok) {
                formTag.reset();
                const newLocation = await response.json();
                console.log(newLocation);
                }
                
            })

        }
    }
    catch (e) {
        console.error("error!", e)
    }
});


