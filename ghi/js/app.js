
const alertDiv = document.querySelector('.alert.alert-warning')
console.log(alertDiv) //wasn't working for joyce but it apparently works?



function createCard(name, description, pictureUrl, datestart, dateend, loc) {

    const startdate = new Date(datestart).toLocaleDateString('en-US')
    const enddate = new Date(dateend).toLocaleDateString('en-US')

    return `
      <div class="card shadow-md p-3 mb-5 bg-body-tertiary rounded">
      <img src="${pictureUrl}" class="card-img-top" object-fit: cover;>
      <div class="card-body">
        <h5 class="card-title">${name}</h5>
        <h5 class="card-subtitle mb-2 text-muted">${loc}</h5>
        <p class="card-text">${description}</p>
      </div>
      <div class="card-footer">${startdate} - ${enddate}</div>
      </div>
      `;
  }


window.addEventListener('DOMContentLoaded', async () => {

  const url = 'http://localhost:8000/api/conferences/';

  try {
    const response = await fetch(url);

    if (!response.ok) {
      // Figure out what to do when the response is bad
      console.error("Error!!!")
      alertDiv.classList.remove("d-none");
      alertDiv.classList.add("d-block");
    } else {
      const data = await response.json();
      let count = 0
      for (let conference of data.conferences) {
        const detailUrl = `http://localhost:8000${conference.href}`;
        const detailResponse = await fetch(detailUrl);
        if (detailResponse.ok) {
            const details = await detailResponse.json();
            const name = details.conference.name;
            const description = details.conference.description;
            const pictureUrl = details.conference.location.picture_url;
            const datestart = details.conference.starts;
            const dateend = details.conference.ends;
            const loc = details.conference.location.name
            const html = createCard(name, description, pictureUrl, datestart, dateend, loc)
            const column = document.querySelectorAll('.col');
            column[count].innerHTML += html;
            count += 1
            if (count === column.length) {
                count = 0
            }
        }
      }
    }
  } catch (e) {
    // Figure out what to do if an error is raised
    console.error('error', e)
    alertDiv.classList.remove("d-none");
    alertDiv.classList.add("d-block");

  }

});


