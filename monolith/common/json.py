from json import JSONEncoder
from django.db.models import QuerySet
from datetime import datetime

# JSONEncoder doesn't know how to do dates
class DateEncoder(JSONEncoder):
    def default(self, o):
        if isinstance(o, datetime):
            return o.isoformat()
        else:
            #
            return super().default(o)


class QuerySetEncoder(JSONEncoder):
    def default(self, o):
        if isinstance(o, QuerySet):
            #turns queryset into a pythonlist, json knows what to do with list
            return list(o)
        else:
            return super().default(o)

#takes properties, attributes, and methods, convert them into JSON
class ModelEncoder(DateEncoder, QuerySetEncoder, JSONEncoder):
    encoders = {}

    def default(self, o):
        if isinstance(o, self.model):
            d = {}
            #checks if a class 'o' has a specific attribute, get_api_url
            if hasattr(o, "get_api_url"): #ex. d
                d["href"] = o.get_api_url() #ex. "href": "/api/conferences/1/""
            #self.properties references the properties attribute in the model classes
            for property in self.properties:
                value = getattr(o, property) #gets value from each property in model class
                if property in self.encoders: #false for now, what we're doing in class don't worry
                    encoder = self.encoders[property]
                    value = encoder.default(value)
                d[property] = value #building the dict d
            #whatever gets returned when calling update, adds to dictionary
            d.update(self.get_extra_data(o))
            #JSON knows how to do this part
            return d
        else:
            return super().default(o)
#api views will override get extra data, will override the return
#defined inside model encoder, doesn't do anything by default
#need to define this bc get_extra_data is called in the default method
    def get_extra_data(self, o):
        return {}
